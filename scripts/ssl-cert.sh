#!/bin/bash
# create a cron jon that copies the ssl cert from wsproxy once a week

local dir=$(basename $(pwd))                                                                                              
mkdir -p /etc/cron.d                                                                                                      
cat <<EOF > /etc/cron.d/"$(echo $dir | tr . -)"-restart                                                                   
# restart the application @$dir each week                                                                                     
0 0 * * 0  root  cs @$dir restart &> /dev/null                                                                                
EOF   

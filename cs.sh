#!/bin/bash

rename_function cmd_remove orig_cmd_remove
cmd_remove() {
    rm -f /etc/cron.d/"$(echo $(basename $(pwd)) | tr . -)"-copy-ssl-cert
    orig_cmd_remove
}

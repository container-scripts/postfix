cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    cs copy-ssl-cert
    _create_cron_job_to_copy_ssl_cert

    cs inject ubuntu-fixes.sh

    cs inject postfix.sh
    cs inject spf-policy.sh
    cs inject opendkim.sh

    for domain in $(cat config/virtual_alias_domains | xargs); do
        cs dkimkey add $domain
    done

    cs inject update.sh
}

# create a cron job that copies the ssl cert from wsproxy once a week
_create_cron_job_to_copy_ssl_cert() {
    local dir=$(basename $(pwd))
    mkdir -p /etc/cron.d
    cat <<EOF > /etc/cron.d/"$(echo $dir | tr . -)"-copy-ssl-cert
# copy the ssl cert @$dir each week
0 0 * * 0  root  cs @$dir copy-ssl-cert &> /dev/null
EOF
}

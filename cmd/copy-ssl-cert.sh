cmd_copy-ssl-cert_help() {
    cat <<_EOF
    copy-ssl-cert
        Copy the SSL cert from 'wsproxy/letsencrypt/' to 'ssl/'

_EOF
}

cmd_copy-ssl-cert() {
    local ssl_cert_dir=$CONTAINERS/wsproxy/letsencrypt/live/$DOMAIN
    mkdir -p ssl
    cp -fL $ssl_cert_dir/fullchain.pem ssl/
    cp -fL $ssl_cert_dir/privkey.pem ssl/
}
